package com.rondaugherty.bottlerocketchallenge.repository

import com.rondaugherty.bottlerocketchallenge.MainApplication
import com.rondaugherty.bottlerocketchallenge.db.StoreDataBase
import com.rondaugherty.bottlerocketchallenge.db.StoreDataDao
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import com.rondaugherty.bottlerocketchallenge.networking.WebService
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject.create
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.wtf

class StoreRepository : AnkoLogger {
    private val storeDataBase: StoreDataBase = StoreDataBase.getInstance(MainApplication.applicationContext())
    private val storeDataDao: StoreDataDao by lazy { storeDataBase.storeDataDao() }
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }


    private fun getStoresFromAPI(): Observable<List<StoreX>> {
        return create { emitter ->
            WebService.getAPIService().getStores()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeBy(
                    onSuccess = ({ response ->
                        response.body()?.stores?.let {
                            emitter.onNext(it)
                            info("store is $it}")
                        }

                    }),
                    onError = ({ wtf("Error with fetching store data response ${it.printStackTrace()}") }),
                    onComplete = ({ info("fetching store data complete") })
                )

        }

    }

    private fun getStoresFromDb(): Observable<List<StoreX>> {
        return storeDataDao.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnNext { info("the data pulled from the database is $it") }
            .doOnError {
                wtf("the db error is ${it.message} , ${it.stackTrace}")
            }
    }

    fun getStores(): Observable<List<StoreX>> {
        return Observable.concat(
            getStoresFromDb(),
            getStoresFromAPI()

        )
    }


    fun insertStoreData(stores: List<StoreX>) {

        val disposable = Observable.fromCallable { storeDataDao.insertAll(stores) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe {
                info("Inserted ${stores.size} stores from API in DB...")
            }

        compositeDisposable.add(disposable)

    }

    fun databaseStatus() = storeDataBase.isOpen


}
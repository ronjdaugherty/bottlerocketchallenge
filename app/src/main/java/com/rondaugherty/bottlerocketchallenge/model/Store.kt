package com.rondaugherty.bottlerocketchallenge.model


import com.google.gson.annotations.SerializedName


data class Store(

    @SerializedName("stores")
    var stores: List<StoreX>
)
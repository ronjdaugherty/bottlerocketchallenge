package com.rondaugherty.bottlerocketchallenge.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity
data class StoreX(
    @SerializedName("storeID")
    @PrimaryKey var storeID: String,
    @SerializedName("address")
    var address: String,
    @SerializedName("city")
    var city: String,
    @SerializedName("latitude")
    var latitude: Double,
    @SerializedName("longitude")
    var longitude: Double,
    @SerializedName("name")
    var name: String,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("state")
    var state: String,
    @SerializedName("storeLogoURL")
    var storeLogoURL: String,
    @SerializedName("zipcode")
    var zipcode: String
)

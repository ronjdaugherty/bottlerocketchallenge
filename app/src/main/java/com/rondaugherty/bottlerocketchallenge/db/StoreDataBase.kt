package com.rondaugherty.bottlerocketchallenge.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rondaugherty.bottlerocketchallenge.model.StoreX

@Database(entities = [StoreX::class], version = 1)
abstract class StoreDataBase: RoomDatabase() {

    abstract fun storeDataDao() : StoreDataDao

    companion object {
        @Volatile private var INSTANCE: StoreDataBase? = null

        fun getInstance (context: Context) : StoreDataBase{
            return INSTANCE ?: synchronized(this){
                                INSTANCE ?: buildDatabase(context).also{ INSTANCE = it}
            }
        }



        private fun buildDatabase(context: Context) : StoreDataBase {
           return Room.databaseBuilder(context.applicationContext, StoreDataBase::class.java, "store_database").build()
        }




}



}
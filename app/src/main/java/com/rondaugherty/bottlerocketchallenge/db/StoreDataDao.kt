package com.rondaugherty.bottlerocketchallenge.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import io.reactivex.Observable

@Dao
interface StoreDataDao {
    @Query("SELECT * FROM StoreX")
    fun getAll(): Observable<List<StoreX>>

    @Insert(onConflict = REPLACE)
    fun insertAll(stores: List<StoreX>)


}
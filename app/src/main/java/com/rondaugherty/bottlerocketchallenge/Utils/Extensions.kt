package com.rondaugherty.bottlerocketchallenge.Utils

import android.content.Context
import android.net.ConnectivityManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rondaugherty.bottlerocketchallenge.model.StoreX

fun Context.isConnectedToInternet(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo

      return when {
          activeNetwork != null && activeNetwork.isConnected -> true
        else -> false
    }
}

fun StoreX.toBundle(storex: StoreX) : String {
    val gson = Gson()
    val type = object : TypeToken<StoreX>() {}.type
    return gson.toJson(storex, type)
}
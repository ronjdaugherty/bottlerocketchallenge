package com.rondaugherty.bottlerocketchallenge.activites

import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.rondaugherty.bottlerocketchallenge.R
import com.rondaugherty.bottlerocketchallenge.Utils.isConnectedToInternet
import com.rondaugherty.bottlerocketchallenge.Utils.toBundle
import com.rondaugherty.bottlerocketchallenge.adapter.StoreAdapter
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import com.rondaugherty.bottlerocketchallenge.repository.StoreRepository
import com.rondaugherty.bottlerocketchallenge.viewmodel.StoreViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.find
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor

class MainActivity : AppCompatActivity(), AnkoLogger {
    private lateinit var storeViewModel: StoreViewModel
    private val storeRepository: StoreRepository by lazy { StoreRepository() }
    private lateinit var storeAdapter: StoreAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        storeRecyclerView.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                context,
                RecyclerView.VERTICAL,
                false
            )
        }

        storeViewModel = ViewModelProviders.of(this).get(StoreViewModel::class.java)

        getStoreViewModel()


    }

    private fun getStoreViewModel() {
        storeViewModel.getStoreData().observe(this, Observer {storeList ->
            info("The store info in the viewModel is $storeList")

            if (!this.isConnectedToInternet()) {
                showSnackbarMessages("The internet is not available")
            }
            if (!storeRepository.databaseStatus()) {
                showSnackbarMessages("The cache is not available ")
            }


            storeList?.let {
                storeAdapter = StoreAdapter(storeList = storeList, context = this ){ store: StoreX -> storeItemClicked (store)  }
                storeRecyclerView.adapter = storeAdapter
            }

            storeRepository.insertStoreData(stores = storeList)

        })
    }

    private fun storeItemClicked (storeX: StoreX) {
        startActivity(intentFor<MapsActivity>("store" to storeX.toBundle(storeX)))

    }

    private fun showSnackbarMessages(message: String) {
        val snackBar = Snackbar.make(
            dashboardCoordinatorLayout,
            message,
            Snackbar.LENGTH_LONG
        )
        val snackBarView = snackBar.view
        val snackTextView = snackBarView.find<TextView>(com.google.android.material.R.id.snackbar_text)
        snackTextView.gravity = Gravity.CENTER

        snackBar.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        storeAdapter.clearObserver()
        storeViewModel.cleanObservers()
    }
}

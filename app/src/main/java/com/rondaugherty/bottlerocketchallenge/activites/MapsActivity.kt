package com.rondaugherty.bottlerocketchallenge.activites

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.rondaugherty.bottlerocketchallenge.R
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_item_row.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, AnkoLogger {

    private val gson: Gson = Gson()
    private lateinit var storeX: StoreX
    private lateinit var storeLocation: LatLng
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        intent?.let {
            val thing = it.extras?.getString("store")
            storeX = gson.fromJson(thing, StoreX::class.java)
            info("the store is ${storeX.name}")
            storeLocation = LatLng(storeX.latitude, storeX.longitude)

            phoneNumberTextView.text = storeX.phone
            addressTextView.text = storeX.address
            cityTextView.text = "${storeX.city}, "
            nameTextView.text = storeX.name
            stateTextView.text = " ${storeX.state}"
            zipCodeTextView.text = storeX.zipcode

            val path = storeX.storeLogoURL
            val uri = Uri.parse(path)


            val disposable = Observable.fromCallable { Picasso.with(this) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { picasso ->
                    picasso.load(uri).into(imageView)
                }

            compositeDisposable.add(disposable)

        }


    }


    override fun onMapReady(googleMap: GoogleMap) {

        val ZOOM_LEVEL = 15f

        with(googleMap) {
            moveCamera(CameraUpdateFactory.newLatLngZoom(storeLocation, ZOOM_LEVEL))
            addMarker(MarkerOptions().position(storeLocation))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}

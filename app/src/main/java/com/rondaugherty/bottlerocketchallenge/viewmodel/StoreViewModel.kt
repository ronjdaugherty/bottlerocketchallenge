package com.rondaugherty.bottlerocketchallenge.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import com.rondaugherty.bottlerocketchallenge.repository.StoreRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.wtf

class StoreViewModel : ViewModel(), AnkoLogger {
    private val storeRepository: StoreRepository = StoreRepository()
    private val storeLiveData : MutableLiveData<List<StoreX>> = MutableLiveData()
    private val compositeDisposable = CompositeDisposable()

    fun getStoreData() : MutableLiveData<List<StoreX>> {
        val disposable = storeRepository.getStores()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .distinctUntilChanged()
            .subscribeBy(
                onNext = ({ store ->
                    storeLiveData.postValue(store)
                }),
                onError = ({ wtf("Error with fetching store data response ${it.printStackTrace()}") }),
                onComplete = ({ info("fetching store data complete") })
            )
        compositeDisposable.add(disposable)

        return storeLiveData

    }

    fun cleanObservers() = compositeDisposable.clear()

}
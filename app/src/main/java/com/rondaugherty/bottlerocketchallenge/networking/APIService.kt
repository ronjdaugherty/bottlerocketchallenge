package com.rondaugherty.bottlerocketchallenge.networking

import com.rondaugherty.bottlerocketchallenge.model.Store
import io.reactivex.Maybe
import retrofit2.Response
import retrofit2.http.GET

interface APIService {

    @GET("stores.json")
    fun getStores() : Maybe<Response<Store>>

}
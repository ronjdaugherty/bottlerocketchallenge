package com.rondaugherty.bottlerocketchallenge

import android.app.Application

class MainApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : MainApplication{
            return instance as MainApplication
        }
    }

    override fun onCreate() {
        super.onCreate()
        // initialize for any

        // Use ApplicationContext.
        // example: SharedPreferences etc...

    }
}
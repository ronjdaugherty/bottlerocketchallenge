package com.rondaugherty.bottlerocketchallenge.adapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rondaugherty.bottlerocketchallenge.R
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_item_row.view.*

class StoreAdapter(
    private var storeList: List<StoreX>,
    private val context: Context,
    private val clickListener: (StoreX) -> Unit
) : androidx.recyclerview.widget.RecyclerView.Adapter<StoreAdapter.StoreViewHolder>() {
    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.store_item_row, parent, false)
        return StoreViewHolder(itemView)
    }

    override fun getItemCount(): Int = storeList.size

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val path = storeList[position].storeLogoURL
        val uri = Uri.parse(path)


        val disposable = Observable.fromCallable { Picasso.with(context) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                it.load(uri).into(holder.itemView.imageView)
            }

        compositeDisposable.add(disposable)
        (holder).bindItems(storeList[position], clickListener)
    }

    fun clearObserver() = compositeDisposable.clear()

    inner class StoreViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        fun bindItems(store: StoreX, clickListener: (StoreX) -> Unit) {

            itemView.phoneNumberTextView.text = store.phone
            itemView.addressTextView.text = store.address
            itemView.cityTextView.text = "${store.city}, "
            itemView.nameTextView.text = store.name
            itemView.stateTextView.text = " ${store.state}"
            itemView.zipCodeTextView.text = store.zipcode

            itemView.setOnClickListener {
                clickListener(store)
            }
        }
    }
}

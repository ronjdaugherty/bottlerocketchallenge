package com.rondaugherty.bottlerocketchallenge.repository

import com.google.common.truth.Truth.assertThat
import com.rondaugherty.bottlerocketchallenge.model.StoreX
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test

class StoreListRepositoryTest {

    private val storeRepository = mockk<StoreRepository>(relaxed = true)
    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun getStores() {
        val testObserver = TestObserver<List<StoreX>>()
        val result = storeRepository.getStores()

        every { storeRepository.getStores() } returns result
        storeRepository.getStores()


        result.subscribe(testObserver)
        testObserver.assertNoErrors()
        testObserver.assertNoTimeout()
        testObserver.hasSubscription()

        verify (atLeast = 1){storeRepository.getStores()  }

        assertThat(storeRepository.getStores()).isNotNull()
    }
}